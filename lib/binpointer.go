package lib

import (
	"bytes"
	"encoding/gob"
)

type PointerType int

const (
	Directory PointerType = iota
	Concrete
)

func init()  {
	gob.Register(&BinPointer{})
}

type BinPointer struct {
	kind              PointerType
	directoryBin      *DirectoryBin
	concreteBin       *ConcreteBin
	concreteSuffix    string
	concreteRawSuffix []byte
}

func NewBinPointerFromDirectoryBin(directoryBin *DirectoryBin) *BinPointer {
	newP := &BinPointer{kind: Directory, directoryBin: directoryBin}
	return newP
}

func NewBinPointerFromConcreteBin(suffix []byte, concreteBin *ConcreteBin) *BinPointer {
	newP := &BinPointer{kind: Concrete, concreteBin: concreteBin,
		concreteSuffix: ToHex(suffix), concreteRawSuffix: suffix}
	return newP
}

func (bp *BinPointer) GobEncode() ([]byte, error) {
	var buf bytes.Buffer
	encoder := gob.NewEncoder(&buf)
	if err := encoder.Encode(bp.kind); err != nil {
		return nil, err
	}
	switch bp.kind {
	case Directory:
		if err := encoder.Encode(bp.directoryBin); err != nil {
			return nil, err
		}
	case Concrete:
		if err := encoder.Encode(bp.concreteBin); err != nil {
			return nil, err
		}
		if err := encoder.Encode(bp.concreteSuffix); err != nil {
			return nil, err
		}
		if err := encoder.Encode(bp.concreteRawSuffix); err != nil {
			return nil, err
		}
	}
	return buf.Bytes(), nil
}

func (bp *BinPointer) GobDecode(b []byte) error {
	buf := bytes.NewBuffer(b)
	decoder := gob.NewDecoder(buf)
	if err := decoder.Decode(&bp.kind); err != nil {
		return err
	}
	switch bp.kind {
	case Directory:
		if err := decoder.Decode(&bp.directoryBin); err != nil {
			return err
		}
	case Concrete:
		if err := decoder.Decode(&bp.concreteBin); err != nil {
			return err
		}
		if err := decoder.Decode(&bp.concreteSuffix); err != nil {
			return err
		}
		if err := decoder.Decode(&bp.concreteRawSuffix); err != nil {
			return err
		}
	}
	return nil
}

func (bp *BinPointer) Kind() PointerType {
	return bp.kind
}

func (bp *BinPointer) DirectoryBin() *DirectoryBin {
	return bp.directoryBin
}

func (bp *BinPointer) ConcreteBin() *ConcreteBin {
	return bp.concreteBin
}

func (bp *BinPointer) ConcreteSuffix() string {
	return bp.concreteSuffix
}

func (bp *BinPointer) ConcreteRawSuffix() []byte {
	return bp.concreteRawSuffix
}

func (bp *BinPointer) Encode() ([]byte, error) {
	var buf bytes.Buffer
	encoder := gob.NewEncoder(&buf)
	if err := encoder.Encode(bp.kind); err != nil {
		return nil, err
	}
	switch bp.kind {
	case Directory:
		if err := encoder.Encode(bp.directoryBin.address); err != nil {
			return nil, err
		}
	case Concrete:
		if err := encoder.Encode(bp.concreteBin.Did()); err != nil {
			return nil, err
		}
		if err := encoder.Encode(bp.concreteSuffix); err != nil {
			return nil, err
		}
		if err := encoder.Encode(bp.concreteRawSuffix); err != nil {
			return nil, err
		}
	}
	return buf.Bytes(), nil
}
