package lib

import (
	"sync"
	"sync/atomic"
)

type Counter struct {
	mtx        sync.Mutex
	opcount    uint64
	onStart    func()
	onComplete func()
}

func NewCounter(onStart func(), onComplete func()) *Counter {
	return &Counter{onStart: onStart, onComplete: onComplete}
}

func (oc *Counter) StartOperation() bool {
	return oc.doStartOperation(true)
}

func (oc *Counter) doStartOperation(lock bool) bool {
	if atomic.AddUint64(&oc.opcount, 1) == 1 {
		if oc.onStart != nil {
			if lock {
				oc.mtx.Lock()
				defer oc.mtx.Unlock()
			}
			(oc.onStart)()
			return true
		}
	}
	return false
}

func (oc *Counter) Lock() bool {
	oc.mtx.Lock()
	return oc.doStartOperation(false)
}

func (oc *Counter) OperationComplete() bool {
	return oc.doOperationComplete(true)
}

func (oc *Counter) doOperationComplete(lock bool) bool {
	if atomic.AddUint64(&oc.opcount, ^uint64(0)) == 0 {
		if oc.onComplete != nil {
			if lock {
				oc.mtx.Lock()
				defer oc.mtx.Unlock()
			}
			(oc.onComplete)()
			return true
		}
	}
	return false
}

func (oc *Counter) Unlock() bool {
	result := oc.doOperationComplete(false)
	oc.mtx.Unlock()
	return result
}

func (oc *Counter) Busy() bool {
	return atomic.LoadUint64(&oc.opcount) > 0
}
