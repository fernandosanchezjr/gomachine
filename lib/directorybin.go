package lib

import (
	"time"
	"path"
	"github.com/boltdb/bolt"
	"bytes"
	"encoding/gob"
)

var directoryTimeout time.Duration = time.Millisecond * 100
var maxFilehandles uint32 = 512
var basePath string = "/opt/gomachine3"

func init()  {
	gob.Register(&DirectoryBin{})
}

type DirectoryBin struct {
	*OperationLoop
	parent    *DirectoryBin
	db        *bolt.DB
	children  map[byte]*BinPointer
	address   []byte
	dbHandler bool
	dbPath    string
}

func NewDirectoryBin(address []byte, parent *DirectoryBin) *DirectoryBin {
	if address == nil {
		address = []byte{}
	}
	newBin := &DirectoryBin{address: address, parent: parent,
		children: make(map[byte]*BinPointer)}
	newBin.OperationLoop = NewOperationLoop(newBin, directoryTimeout)
	if !newBin.IsRoot() {
		newBin.InitializeDB()
	}
	return newBin
}

func NewRootDirectoryBin(name string) *DirectoryBin {
	newBin := NewDirectoryBin(nil, nil)
	newBin.dbPath = path.Join(basePath, name)
	newBin.InitializeDB()
	return newBin
}

func (d *DirectoryBin) GobEncode() ([]byte, error) {
	var buf bytes.Buffer
	encoder := gob.NewEncoder(&buf)
	if err := encoder.Encode(d.dbHandler); err != nil {
		return nil, err
	}
	if err := encoder.Encode(d.address); err != nil {
		return nil, err
	}
	if err := encoder.Encode(d.children); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func (d *DirectoryBin) GobDecode(b []byte) error {
	buf := bytes.NewBuffer(b)
	decoder := gob.NewDecoder(buf)
	if err := decoder.Decode(&d.dbHandler); err != nil {
		return err
	}
	if err := decoder.Decode(&d.address); err != nil {
		return err
	}
	if err := decoder.Decode(&d.children); err != nil {
		return err
	}
	return nil
}

func (d *DirectoryBin) GetDBPath() string {
	return path.Join(d.dbPath, "bolt.db")
}

func (d *DirectoryBin) InitializeDB() {
	if !d.IsRoot() {
		if value, parsed := ToUint32(d.address); parsed && (value <= maxFilehandles) {
			pathComponents := []string{d.FetchRoot().dbPath}
			pathComponents = append(pathComponents, ToHexArray(d.address)...)
			d.dbPath = path.Join(pathComponents...)
			d.dbHandler = true
		}
	} else {
		d.dbHandler = true
	}
	if d.dbHandler {
		CreatePath(d.dbPath, 0750)
		if db, err := bolt.Open(d.GetDBPath(), 0660, nil); err != nil {
			panic(err)
		} else {
			d.db = db
		}
	}
}

func (d *DirectoryBin) FetchDbHandler() *DirectoryBin {
	if d.dbHandler {
		return d
	} else {
		return d.parent.FetchDbHandler()
	}
}

func (d *DirectoryBin) IsRoot() bool {
	return len(d.address) == 0
}

func (d *DirectoryBin) FetchRoot() *DirectoryBin {
	if d.IsRoot() {
		return d
	} else {
		return d.parent.FetchRoot()
	}
}

func (d *DirectoryBin) ChildCount() int {
	return len(d.children)
}

func (d *DirectoryBin) Id() (byte, bool) {
	if d.IsRoot() {
		return 0x00, false
	} else {
		return d.address[len(d.address)-1], true
	}
}

func (d *DirectoryBin) Walk(did Did) *ConcreteBin {
	wo := NewWalkOperation(did.Hash(), did)
	d.Execute(wo)
	return <-wo.Response
}

func (d *DirectoryBin) addNewConcreteChild(did Did, next byte, rest []byte) *ConcreteBin {
	newBin := NewConcreteBin(did, d)
	newPointer := NewBinPointerFromConcreteBin(rest, newBin)
	d.children[next] = newPointer
	return newBin
}

func (d *DirectoryBin) addExistingConcreteChild(concrete *ConcreteBin, next byte, rest []byte) {
	newPointer := NewBinPointerFromConcreteBin(rest, concrete)
	d.children[next] = newPointer
}

func (d *DirectoryBin) upgradeToDirectoryBin(concrete *ConcreteBin, next byte, pointer *BinPointer,
	did Did) *DirectoryBin {
	otherNext, otherRest := pointer.ConcreteRawSuffix()[0], pointer.ConcreteRawSuffix()[1:]
	newDirectory := NewDirectoryBin(d.childHash(next), d)
	d.children[next] = NewBinPointerFromDirectoryBin(newDirectory)
	newDirectory.addExistingConcreteChild(concrete, otherNext, otherRest)
	return newDirectory
}

func (d *DirectoryBin) childHash(next byte) []byte {
	newHash := make([]byte, 0, len(d.address)+1)
	newHash = append(newHash, d.address...)
	newHash = append(newHash, next)
	return newHash
}

func (d *DirectoryBin) ExecuteOperation(op Operation) {
	op.Perform(d)
}

func (d *DirectoryBin) Execute(op Operation) {
	d.StartOperation()
	defer d.OperationComplete()
	d.OperationLoop.Execute(op)
}

func (d *DirectoryBin) LoopStarted() {
	if d.parent != nil {
		d.parent.StartOperation()
	}
}

func (d *DirectoryBin) LoopComplete() {
	//d.Persist(d)
	if d.parent != nil {
		d.parent.OperationComplete()
	}
}

func (d *DirectoryBin) Persist(target *DirectoryBin) {
	if d.dbHandler {
		if err := d.db.Batch(func (tx *bolt.Tx) error {
			bucket, err := tx.CreateBucketIfNotExists(target.address)
			if err != nil {
				return nil
			}
			var buf bytes.Buffer
			encoder := gob.NewEncoder(&buf)
			encoder.Encode(target)
			if err = bucket.Put(d.address, buf.Bytes()); err != nil {
				return err
			}
			return nil
		}); err != nil {
			panic(err)
		}
	} else {
		d.FetchDbHandler().Persist(target)
	}
}