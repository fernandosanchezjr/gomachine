package lib

import "testing"

func TestRootDirectory(t *testing.T) {
	rootBin := NewRootDirectoryBin("rootTest")
	if rootBin.IsRoot() == false {
		t.Fatal("Root bin detection error")
	}
	if _, ok := rootBin.Id(); ok {
		t.Fatal("Root bin seems to have an Id")
	}
	nonRootBin := NewDirectoryBin([]byte{12}, rootBin)
	if nonRootBin.IsRoot() == true {
		t.Fatal("Non-root bin detection error")
	}
	if _, ok := nonRootBin.Id(); !ok {
		t.Fatal("Non-root bin seems to not have an Id")
	}
}

func TestDirectoryWalk(t *testing.T) {
	didCount := 1000
	rootBin := NewRootDirectoryBin("walkTest")
	for i := 0; i < didCount; i++ {
		result := rootBin.Walk(NewDid(i))
		if result == nil {
			t.Fatal("No bin received from Walk")
		}
	}
	rootBin.Wait()
}

func TestDirectoryOperationLoop(t *testing.T) {
	rootBin := NewRootDirectoryBin("loopTest")
	to := NewTestOperation("hello world", false)
	rootBin.Execute(to)
	result := <- to.Result
	if result != 123 {
		t.Fatal("Invalid result from TestOperation")
	}
	rootBin.Wait()
}
