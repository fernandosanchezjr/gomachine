package lib

type TestOperation struct {
	Param1 string
	Param2 bool
	Result chan int
}

func NewTestOperation(Param1 string, Param2 bool) *TestOperation {
	return &TestOperation{Param1: Param1, Param2: Param2, Result: make(chan int)}
}

func (to *TestOperation) Perform(host OperationHost) {
	directory := host.(*DirectoryBin)
	println("Executing in", directory.address, "with", to.Param1, to.Param2)
	to.Result <- 123
}
