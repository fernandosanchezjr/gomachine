package lib

import (
	"bytes"
	"encoding/gob"
)

func init()  {
	gob.Register(&ConcreteBin{})
}

type ConcreteBin struct {
	directory *DirectoryBin
	did       Did
}

func NewConcreteBin(did Did, directory *DirectoryBin) *ConcreteBin {
	newBin := &ConcreteBin{did: did, directory: directory}
	return newBin
}

func (c *ConcreteBin) Did() Did {
	return c.did
}

func (c *ConcreteBin) GobEncode() ([]byte, error) {
	var buf bytes.Buffer
	encoder := gob.NewEncoder(&buf)
	if err := encoder.Encode(c.did); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func (c *ConcreteBin) GobDecode(b []byte) error {
	buf := bytes.NewBuffer(b)
	decoder := gob.NewDecoder(buf)
	if err := decoder.Decode(&c.did); err != nil {
		return err
	}
	return nil
}