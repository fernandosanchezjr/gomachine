package lib

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"reflect"
	"strings"
	"encoding/binary"
)

type Did []interface{}

type DidSlice []Did

func ToDid(val []interface{}) Did {
	return (Did)(val)
}

func NewDid(val ...interface{}) Did {
	return val
}

func DidString(val []interface{}) string {
	return ToDid(val).String()
}

func (d Did) Length() int {
	return len(d)
}

func (d Did) Root() bool {
	return len(d) == 0
}

func ToDidString(value interface{}) string {
	if s, ok := value.(fmt.Stringer); ok {
		return s.String()
	}
	v := reflect.ValueOf(value)
	switch v.Kind() {
	case reflect.String:
		return fmt.Sprintf("\"%s\"", value)
	case reflect.Struct:
		return fmt.Sprintf("%+#v", v.Interface())
	case reflect.Array, reflect.Slice:
		components := make([]string, v.Len())
		for i := 0; i < v.Len(); i++ {
			components[i] = ToDidString(v.Index(i).Interface())
		}
		return fmt.Sprintf("%s{%s}", v.Type(), strings.Join(components, ", "))
	case reflect.Ptr:
		return ToDidString(v.Elem().Interface())
	default:
		return fmt.Sprint(value)
	}
}

func (d Did) String() string {
	return ToDidString(([]interface{})(d))
}

func (d Did) Hash() []byte {
	hasher := sha256.New()
	hasher.Write([]byte(d.String()))
	return hasher.Sum(nil)
}

func (d Did) Path() string {
	hexChunks := make([]string, 0)
	didHash := d.Hash()
	var next []byte
	for {
		next, didHash = didHash[:1], didHash[1:]
		hexChunks = append(hexChunks, hex.EncodeToString(next))
		if len(didHash) == 0 {
			break
		}
	}
	return strings.Join(hexChunks, "/")
}

func (d Did) Equals(od Did) bool {
	if d.Length() != od.Length() {
		return false
	}
	return d.String() == od.String()
}

func (d Did) StartsWith(od Did) bool {
	if d.Length() < od.Length() {
		return false
	}
	for pos, value := range od {
		if value != d[pos] {
			return false
		}
	}
	return true
}

func (d Did) Append(id interface{}) Did {
	childDid := make([]interface{}, 0, len(d)+1)
	childDid = append(childDid, d...)
	return append(childDid, id)
}

func (d Did) Prepend(id interface{}) Did {
	childDid := make([]interface{}, 1, len(d)+1)
	childDid[0] = id
	return append(childDid, d...)
}

func (d Did) Id() interface{} {
	if didLen := len(d); didLen == 0 {
		return nil
	} else {
		return d[didLen-1]
	}
}

func (ds DidSlice) Contains(od Did) bool {
	for _, d := range ds {
		if d.Equals(od) {
			return true
		}
	}
	return false
}

func (ds DidSlice) StartWith(od Did) DidSlice {
	retDs := make(DidSlice, 0)
	for _, d := range ds {
		if d.StartsWith(od) {
			retDs = append(retDs, d)
		}
	}
	return retDs
}

func DidEquals(a, b Did) bool {
	return a.Equals(b)
}

func DidStartsWith(a, b Did) bool {
	return a.StartsWith(b)
}

func DidSliceContains(a Did, ds DidSlice) bool {
	return ds.Contains(a)
}

func DidSliceStartWith(a Did, ds DidSlice) DidSlice {
	return ds.StartWith(a)
}

func DidSliceLeft(d Did, l int) Did {
	return d[l:]
}

func DidSliceRight(d Did, r int) Did {
	return d[:r]
}

func DidSliceLeftRight(d Did, l int, r int) Did {
	return d[l:r]
}

func DidAppend(d Did, id interface{}) Did {
	return d.Append(id)
}

func DidPrepend(d Did, id interface{}) Did {
	return d.Prepend(id)
}

func ToHex(b []byte) string {
	return hex.EncodeToString(b)
}

func ToUint32(b []byte) (uint32, bool) {
	switch len(b) {
	case 1:
		return binary.BigEndian.Uint32([]byte{0, 0, 0, b[0]}), true
	case 2:
		return binary.BigEndian.Uint32([]byte{0, 0, b[1], b[0]}), true
	case 3:
		return binary.BigEndian.Uint32([]byte{0, b[2], b[1], b[0]}), true
	case 4:
		return binary.BigEndian.Uint32([]byte{b[3], b[2], b[1], b[0]}), true
	default:
		return 0, false
	}
}

func ToHexArray(b []byte) []string {
	result := make([]string, 0, len(b))
	for _, value := range(b) {
		result = append(result, hex.EncodeToString([]byte{value}))
	}
	return result
}