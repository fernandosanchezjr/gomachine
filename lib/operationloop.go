package lib

import (
	"time"
	"sync"
)

type Operation interface {
	Perform(OperationHost)
}

type OperationHost interface {
	ExecuteOperation(Operation)
	LoopStarted()
	LoopComplete()
}

type OperationLoop struct {
	*Counter
	waiter  *Wait
	channel chan Operation
	host    OperationHost
	timeout time.Duration
	started bool
	mtx     sync.Mutex
}

func NewOperationLoop(host OperationHost, timeout time.Duration) *OperationLoop {
	ol := &OperationLoop{channel: make(chan Operation, 1), host: host, timeout: timeout, waiter: NewWait()}
	ol.Counter = NewCounter(ol.onStart, ol.onComplete)
	return ol
}

func (ol *OperationLoop) start() {
	ol.mtx.Lock()
	defer ol.mtx.Unlock()
	if ol.started {
		return
	}
	started := make(chan int, 1)
	go ol.run(started)
	<- started
}

func (ol *OperationLoop) Execute(op Operation) {
	ol.start()
	ol.channel <- op
}

func (ol *OperationLoop) run(started chan int) {
	ol.host.LoopStarted()
	ol.started = true
	started <- 1
	for {
		select {
		case op := <-ol.channel:
			if op != nil {
				ol.host.ExecuteOperation(op)
			}
		case <-time.After(ol.timeout):
			if len(ol.channel) == 0 {
				if !ol.Busy() {
					ol.started = false
					ol.host.LoopComplete()
					ol.waiter.Notify()
					return
				}
			}
		}
	}
}

func (ol *OperationLoop) onStart() {

}

func (ol *OperationLoop) onComplete() {

}

func (ol *OperationLoop) Wait() {
	if !ol.Busy() {
		return
	}
	ol.waiter.Wait()
}
