package lib

import (
	"os"
	"sync"
)

var pathMutex sync.Mutex

func CreatePath(p string, perms os.FileMode) {
	pathMutex.Lock()
	defer pathMutex.Unlock()
	err := os.MkdirAll(p, perms)
	if err != nil {
		panic(err)
	}
}
