package lib

import (
	"errors"
	"fmt"
)

type WalkOperation struct {
	Address []byte
	Did Did
	Response chan *ConcreteBin
}

func NewWalkOperation(Address []byte, Did Did) *WalkOperation {
	wo := &WalkOperation{Address: Address, Did: Did, Response: make(chan *ConcreteBin, 1)}
	return wo
}

func (wo *WalkOperation) Perform(host OperationHost) {
	directory := host.(*DirectoryBin)
	if len(wo.Address) == 0 {
		panic(errors.New("Directory address length must be non-zero"))
	}
	next, rest := wo.Address[0], wo.Address[1:]
	if pointer, ok := directory.children[next]; ok {
		switch pointer.Kind() {
		case Directory:
			wo.Address = rest
			pointer.DirectoryBin().Execute(wo)
		case Concrete:
			hexRest := ToHex(rest)
			concrete := pointer.ConcreteBin()
			if hexRest == pointer.ConcreteSuffix() {
				if wo.Did.Equals(concrete.Did()) {
					wo.Response <- pointer.ConcreteBin()
				} else {
					panic(errors.New(fmt.Sprintln("Hash collision found between", wo.Did.String(), "and",
						concrete.Did(), "at", ToHex(directory.address))))
				}
			} else {
				newDirectory := directory.upgradeToDirectoryBin(concrete, next, pointer, wo.Did)
				wo.Address = rest
				newDirectory.Execute(wo)
			}
		}
	} else {
		wo.Response <- directory.addNewConcreteChild(wo.Did, next, rest)
	}
}